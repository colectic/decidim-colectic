# frozen_string_literal: true

# afegit per configurar direct_verifications
Rails.application.config.direct_verifications_parser = :metadata
